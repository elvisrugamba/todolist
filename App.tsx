import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import DatabaseProvider from './src/context/Database/DatabaseProvider';
import TodoProvider from './src/context/Todo/TodoProvider';
import { AppNavigator } from './src/navigators';
import './src/translations/i18n';

const App = () => (
  <DatabaseProvider>
    <TodoProvider>
      <SafeAreaProvider>
        <NavigationContainer>
          <AppNavigator />
        </NavigationContainer>
      </SafeAreaProvider>
    </TodoProvider>
  </DatabaseProvider>
);

export default App;
