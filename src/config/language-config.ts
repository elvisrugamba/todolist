export const languageConfig = [
  {
    key: 1,
    name: 'English',
    code: 'en',
  },
  {
    key: 2,
    name: 'Kinyarwanda',
    code: 'kn',
  },
];
