import React from 'react';
import { View, StatusBar, TouchableOpacity } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';

import { AppStackParamList } from '../../types/navigator.types';
import { Button, Text } from '../../components';
import i18n from '../../translations/i18n';
import { languageConfig } from '../../config/language-config';
import { Colors } from '../../styles';
import styles from './SettingsScreen.styles';

type SettingsScreenProps = {
  navigation: NativeStackScreenProps<
    AppStackParamList,
    'Settings'
  >['navigation'];
};

const SettingsScreen: React.FC<SettingsScreenProps> = ({ navigation }) => {
  const { t } = useTranslation();

  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng);
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={Colors.WHITE} />
      <View style={styles.content}>
        <Button
          transparent
          onPress={() => navigation.goBack()}
          style={styles.closeBtn}>
          <Icon name="close" color={Colors.DEFAULT} size={28} />
        </Button>
        <View style={styles.heading}>
          <Text headline bold>
            {t('choose_language')}
          </Text>
        </View>
        <View style={styles.languages}>
          {languageConfig.map((lng, index) => (
            <TouchableOpacity
              key={lng.key}
              activeOpacity={0.8}
              onPress={() => changeLanguage(lng.code)}
              style={[
                styles.picker,
                index === languageConfig.length - 1 && styles.borderTop,
              ]}>
              <Text body>{lng.name}</Text>
              {i18n.language === lng.code ? (
                <Icon name="checkmark" color={Colors.DEFAULT} size={20} />
              ) : null}
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SettingsScreen;
