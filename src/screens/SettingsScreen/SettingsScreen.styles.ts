import { StyleSheet } from 'react-native';

import { Colors } from '../../styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 30,
  },
  closeBtn: {
    position: 'absolute',
    top: 0,
    right: 30,
    height: 56,
  },
  heading: {
    alignItems: 'center',
    marginBottom: 20,
  },
  languages: {
    borderWidth: 1,
    borderColor: Colors.LIGHT_GRAY,
    borderRadius: 4,
  },
  picker: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 48,
    paddingHorizontal: 16,
  },
  borderTop: {
    borderTopWidth: 1,
    borderTopColor: Colors.LIGHT_GRAY,
  },
});

export default styles;
