import { StyleSheet } from 'react-native';

import { Colors } from '../../styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 30,
  },
  closeBtn: {
    position: 'absolute',
    top: 0,
    right: 30,
    height: 56,
  },
  heading: {
    alignItems: 'center',
    marginBottom: 20,
  },
  colors: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  picker: {
    width: 28,
    height: 28,
    borderRadius: 4,
  },
  submitBtn: {
    marginTop: 20,
  },
});

export default styles;
