import React from 'react';
import { View, StatusBar, TouchableOpacity } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { AppStackParamList } from '../../types/navigator.types';
import { ListFormValues } from '../../interfaces/form-values.interface';
import { Button, Input, Text } from '../../components';
import { useTodo } from '../../context/Todo';
import { Colors, Fonts } from '../../styles';
import styles from './CreateListScreen.styles';

const COLORS = [
  {
    key: 1,
    value: Colors.GREEN,
  },
  {
    key: 2,
    value: Colors.PRIMARY,
  },
  {
    key: 3,
    value: Colors.BLUE,
  },
  {
    key: 4,
    value: Colors.INDIGO,
  },
  {
    key: 5,
    value: Colors.PURPLE,
  },
  {
    key: 6,
    value: Colors.RED,
  },
  {
    key: 7,
    value: Colors.ORANGE,
  },
];

const FormSchema = Yup.object().shape({
  name: Yup.string().min(2, 'list_name_short').required('list_name_required'),
});

type CreateListScreenProps = {
  navigation: NativeStackScreenProps<
    AppStackParamList,
    'CreateList'
  >['navigation'];
};

const CreateListScreen: React.FC<CreateListScreenProps> = ({ navigation }) => {
  const { t } = useTranslation();
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = useFormik<ListFormValues>({
    initialValues: {
      name: '',
      color: COLORS[0].value,
    },
    validationSchema: FormSchema,
    onSubmit: formValues => {
      createList(formValues);
      resetForm({ values: { name: '', color: COLORS[0].value } });
    },
  });
  const { addTodoList } = useTodo();

  const createList = ({ name, color }: ListFormValues) => {
    addTodoList(name, color);
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={Colors.WHITE} />
      <View style={styles.content}>
        <Button
          transparent
          onPress={() => navigation.goBack()}
          style={styles.closeBtn}>
          <Icon name="close" color={Colors.DEFAULT} size={28} />
        </Button>
        <View style={styles.heading}>
          <Text headline bold>
            {t('create_todo_list')}
          </Text>
        </View>
        <Input
          placeholder={t('list_name')}
          value={values.name}
          onChangeText={handleChange('name')}
          onBlur={handleBlur('name')}
          error={
            touched.name && errors.name
              ? t(errors.name as 'list_name_short' | 'list_name_required')
              : undefined
          }
        />
        <View style={styles.colors}>
          {COLORS.map(clr => (
            <TouchableOpacity
              key={clr.key}
              activeOpacity={0.8}
              onPress={() => setFieldValue('color', clr.value)}
              style={[styles.picker, { backgroundColor: clr.value }]}
            />
          ))}
        </View>
        <Button
          color={values.color}
          textStyle={Fonts.bold}
          onPress={handleSubmit}
          style={styles.submitBtn}>
          {t('create')}
        </Button>
      </View>
    </SafeAreaView>
  );
};

export default CreateListScreen;
