import { StyleSheet } from 'react-native';

import { Colors } from '../../styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
  },
  header: {
    marginLeft: 56,
  },
  list: {
    flex: 1,
    paddingHorizontal: 20,
  },
  listContent: {
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  listEmpty: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: '65%',
  },
  footer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  input: {
    flex: 1,
  },
  submitBtn: {
    width: 48,
    marginVertical: 10,
    marginLeft: 8,
  },
});

export default styles;
