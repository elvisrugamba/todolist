import React, { useState, useEffect } from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { AppStackParamList } from '../../types/navigator.types';
import { Task } from '../../interfaces/todo.interface';
import { TaskFormValues } from '../../interfaces/form-values.interface';
import {
  Button,
  ConfirmModal,
  Input,
  ListEmpty,
  TaskItem,
  TodoTasksHeader,
} from '../../components';
import { useTodo } from '../../context/Todo';
import { Colors } from '../../styles';
import styles from './TodoTasksScreen.styles';

const FormSchema = Yup.object().shape({
  description: Yup.string()
    .min(2, 'description_short')
    .required('description_required'),
});

type TodoTasksScreenProps = {
  navigation: NativeStackScreenProps<
    AppStackParamList,
    'TodoTasks'
  >['navigation'];
  route: NativeStackScreenProps<AppStackParamList, 'TodoTasks'>['route'];
};

const TodoTasksScreen: React.FC<TodoTasksScreenProps> = ({
  navigation,
  route,
}) => {
  const { listId, listName, listColor } = route.params;
  const [tasks, setTasks] = useState<Task[]>([]);
  const [selectedId, setSelectedId] = useState<number>();
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const { t } = useTranslation();
  const { todos, addTodoTask, updateTodoTaskStatus, removeTodoTask } =
    useTodo();
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = useFormik<TaskFormValues>({
    initialValues: {
      description: '',
    },
    validationSchema: FormSchema,
    onSubmit: formValues => {
      createTask(formValues);
      resetForm({ values: { description: '' } });
    },
  });

  useEffect(() => {
    const list = todos.find(todo => todo.id === listId);
    setTasks(list?.tasks ?? []);
  }, [listId, todos]);

  const createTask = ({ description }: TaskFormValues) => {
    addTodoTask(listId, description);
  };

  const updateTaskStatus = (taskId: number, done: boolean) => {
    updateTodoTaskStatus(listId, taskId, done);
  };

  const removeTask = () => {
    selectedId && removeTodoTask(listId, selectedId);
  };

  const onLongPress = (id: number) => {
    setSelectedId(id);
    setShowConfirmModal(true);
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={Colors.WHITE} />
      <View style={styles.container}>
        <TodoTasksHeader
          title={listName}
          color={listColor}
          completed={tasks.filter(task => task.done).length}
          total={tasks.length}
          rightHandler={() => navigation.goBack()}
          style={styles.header}
        />
        <View style={styles.content}>
          <FlatList
            contentContainerStyle={styles.listContent}
            data={tasks}
            keyExtractor={item => `${item.id}`}
            renderItem={({ item }) => (
              <TaskItem
                key={item.id}
                description={item.description}
                done={item.done}
                onToggle={done => updateTaskStatus(item.id, done)}
                onLongPress={() => onLongPress(item.id)}
              />
            )}
            ListEmptyComponent={() => (
              <ListEmpty
                title={t('nothing_here')}
                description={t('start_with_task')}
                style={styles.listEmpty}
              />
            )}
          />
        </View>
        <View style={styles.footer}>
          <Input
            value={values.description}
            onChangeText={handleChange('description')}
            onBlur={handleBlur('description')}
            error={
              touched.description && errors.description
                ? t(
                    errors.description as
                      | 'description_short'
                      | 'description_required',
                  )
                : undefined
            }
            style={styles.input}
          />
          <Button
            color={listColor}
            onPress={handleSubmit}
            style={styles.submitBtn}>
            <Icon name="add" color={Colors.WHITE} size={16} />
          </Button>
        </View>
      </View>
      {showConfirmModal ? (
        <ConfirmModal
          visible={showConfirmModal}
          title={t('remove_task')}
          description={t('are_you_sure_remove_task')}
          close={() => setShowConfirmModal(false)}
          action={removeTask}
        />
      ) : null}
    </SafeAreaView>
  );
};

export default TodoTasksScreen;
