import React, { useState } from 'react';
import { View, ScrollView, FlatList, StatusBar } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import FaIcon from 'react-native-vector-icons/Feather';
import { useTranslation } from 'react-i18next';

import { AppStackParamList } from '../../types/navigator.types';
import {
  Button,
  ConfirmModal,
  ListEmpty,
  ListItem,
  Text,
} from '../../components';
import { useTodo } from '../../context/Todo';
import { Colors } from '../../styles';
import styles from './HomeScreen.styles';

type HomeScreenProps = {
  navigation: NativeStackScreenProps<AppStackParamList, 'Home'>['navigation'];
};

const HomeScreen: React.FC<HomeScreenProps> = ({ navigation }) => {
  const [selectedId, setSelectedId] = useState<number>();
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const { t } = useTranslation();
  const { todos, removeTodoList } = useTodo();

  const onLongPress = (id: number) => {
    setSelectedId(id);
    setShowConfirmModal(true);
  };

  const removeList = () => {
    selectedId && removeTodoList(selectedId);
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={Colors.WHITE} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        contentContainerStyle={styles.container}>
        <View style={styles.content}>
          <Button
            transparent
            onPress={() => navigation.navigate('Settings')}
            style={styles.settingsBtn}>
            <FaIcon name="settings" color={Colors.DEFAULT} size={28} />
          </Button>
          <View style={styles.top}>
            <View style={styles.heading}>
              <View style={styles.hr} />
              <View style={styles.headingText}>
                <Text headline bold>
                  Todo{' '}
                  <Text headline color={Colors.PRIMARY}>
                    Lists
                  </Text>
                </Text>
              </View>
              <View style={styles.hr} />
            </View>
            <View style={styles.add}>
              <Button
                outline
                onPress={() => navigation.navigate('CreateList')}
                style={styles.addBtn}>
                <Icon name="add" color={Colors.PRIMARY} size={16} />
              </Button>
              <Text caption bold color={Colors.PRIMARY}>
                {t('add_list')}
              </Text>
            </View>
          </View>
          <View style={styles.list}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.listContent}
              data={todos}
              keyExtractor={item => `${item.id}`}
              renderItem={({ item }) => (
                <ListItem
                  key={item.id}
                  name={item.name}
                  remaining={item.tasks.filter(value => !value.done).length}
                  completed={item.tasks.filter(value => value.done).length}
                  color={item.color}
                  onPress={() =>
                    navigation.navigate('TodoTasks', {
                      listId: item.id,
                      listName: item.name,
                      listColor: item.color,
                    })
                  }
                  onLongPress={() => onLongPress(item.id)}
                />
              )}
              ListEmptyComponent={() => (
                <ListEmpty
                  title={t('nothing_here')}
                  description={t('start_with_list')}
                  style={styles.listEmpty}
                />
              )}
            />
          </View>
        </View>
      </ScrollView>
      {showConfirmModal ? (
        <ConfirmModal
          visible={showConfirmModal}
          title={t('remove_list')}
          description={t('are_you_sure_remove_list')}
          close={() => setShowConfirmModal(false)}
          action={removeList}
        />
      ) : null}
    </SafeAreaView>
  );
};

export default HomeScreen;
