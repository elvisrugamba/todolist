import { StyleSheet } from 'react-native';

import { Colors, Dimensions } from '../../styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
  },
  settingsBtn: {
    position: 'absolute',
    top: 0,
    right: 30,
    height: 56,
  },
  top: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  heading: {
    width: Dimensions.SCREEN_WIDTH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headingText: {
    alignItems: 'center',
  },
  hr: {
    width: 40,
    height: 2,
    backgroundColor: Colors.LIGHT_GRAY,
  },
  add: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 40,
  },
  addBtn: {
    width: 48,
    marginBottom: 4,
  },
  list: {
    flex: 1,
    paddingHorizontal: 20,
  },
  listContent: {
    // paddingHorizontal: 20,
  },
  listEmpty: {
    width: Dimensions.SCREEN_WIDTH - 20 * 2,
  },
});

export default styles;
