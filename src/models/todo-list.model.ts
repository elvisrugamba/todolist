import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  CreateDateColumn,
} from 'typeorm/browser';

import { TodoTask } from './todo-task.model';

@Entity('lists')
export class TodoList {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  color: string;

  @OneToMany(() => TodoTask, task => task.list)
  tasks: TodoTask[];

  @CreateDateColumn()
  createdDate: Date;
}
