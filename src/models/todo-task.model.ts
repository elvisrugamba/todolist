import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
} from 'typeorm/browser';

import { TodoList } from './todo-list.model';

@Entity('tasks')
export class TodoTask {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column('boolean', { default: false })
  done: string;

  @ManyToOne(() => TodoList, list => list.tasks)
  list: TodoList;

  @CreateDateColumn()
  createdDate: Date;
}
