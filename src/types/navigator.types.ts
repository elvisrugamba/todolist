export type AppStackParamList = {
  Home: undefined;
  CreateList: undefined;
  TodoTasks: {
    listId: number;
    listName: string;
    listColor: string;
  };
  Settings: undefined;
};
