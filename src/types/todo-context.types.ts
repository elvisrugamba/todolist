import { List } from '../interfaces/todo.interface';

export type TodoContextType = {
  todos: List[];
  addTodoList: (name: List['name'], color: List['color']) => void;
  removeTodoList: (id: number) => void;
  addTodoTask: (listId: number, description: string) => void;
  updateTodoTaskStatus: (listId: number, taskId: number, done: boolean) => void;
  removeTodoTask: (listId: number, taskId: number) => void;
};
