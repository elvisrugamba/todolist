import { useContext } from 'react';

import DatabaseContext from './DatabaseContext';

const useDatabase = () => {
  const context = useContext(DatabaseContext);

  if (context === undefined) {
    throw new Error('useDatabase must be used within an DatabaseContext');
  }

  return context;
};

export default useDatabase;
