import { createContext } from 'react';

import { DatabaseContextData } from '../../interfaces/database-context.interface';

const DatabaseContext = createContext<DatabaseContextData>(
  {} as DatabaseContextData,
);

export default DatabaseContext;
