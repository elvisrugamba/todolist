import React, { useState, useEffect, useCallback } from 'react';
import { View } from 'react-native';
import { Connection, createConnection } from 'typeorm/browser';

import DatabaseContext from './DatabaseContext';
import { TodoList } from '../../models/todo-list.model';
import { TodoTask } from '../../models/todo-task.model';
import { TodoListDao } from '../../dao/todo-list.dao';
import { TodoTaskDao } from '../../dao/todo-task.dao';

const DatabaseProvider: React.FC = ({ children }) => {
  const [connection, setConnection] = useState<Connection | null>(null);

  const connect = useCallback(async () => {
    try {
      const createdConnection = await createConnection({
        type: 'react-native',
        database: 'todo',
        location: 'default',
        logging: ['error', 'query', 'schema'],
        synchronize: true,
        entities: [TodoList, TodoTask],
      });
      setConnection(createdConnection);
    } catch (error) {
      console.log(error);
    }
  }, []);

  useEffect(() => {
    if (!connection) {
      connect();
    }
  }, [connect, connection]);

  if (!connection) {
    return <View />;
  }

  const options = {
    todoListDao: new TodoListDao(connection as Connection),
    todoTaskDao: new TodoTaskDao(connection as Connection),
  };

  return (
    <DatabaseContext.Provider value={options}>
      {children}
    </DatabaseContext.Provider>
  );
};

export default DatabaseProvider;
