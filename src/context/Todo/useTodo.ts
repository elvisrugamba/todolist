import { useContext } from 'react';

import { TodoContextType } from '../../types/todo-context.types';
import TodoContext from './TodoContext';

const useTodo = () => {
  const context = useContext(TodoContext) as TodoContextType;

  if (context === undefined) {
    throw new Error('useTodo must be used within an TodoContext');
  }

  return context;
};

export default useTodo;
