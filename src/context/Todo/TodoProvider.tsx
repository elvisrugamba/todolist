import React, { useState } from 'react';
import { List, Task } from '../../interfaces/todo.interface';

import TodoContext from './TodoContext';

const TodoProvider: React.FC = ({ children }) => {
  const [todos, setTodos] = useState<List[]>([]);

  const addTodoList = (name: List['name'], color: List['color']) => {
    const ids: number[] = todos.map(t => t.id);
    const newTodo: List = {
      id: ids.length > 0 ? Math.max(...ids) + 1 : 1,
      tasks: [],
      createdDate: new Date(),
      name,
      color,
    };

    setTodos([...todos, newTodo]);
  };

  const removeTodoList = (id: number) => {
    const newTodos = todos.filter(todo => todo.id !== id);

    setTodos(newTodos);
  };

  const addTodoTask = (listId: number, description: string) => {
    const updatedTodo = todos.map(todo => {
      if (todo.id === listId) {
        const ids: number[] = todo.tasks.map(t => t.id);
        const newTask: Task = {
          id: ids.length > 0 ? Math.max(...ids) + 1 : 1,
          done: false,
          createdDate: new Date(),
          description,
        };

        return { ...todo, tasks: [...todo.tasks, newTask] };
      }

      return todo;
    });

    setTodos(updatedTodo);
  };

  const updateTodoTaskStatus = (
    listId: number,
    taskId: number,
    done: boolean,
  ) => {
    const updatedTodo = todos.map(todo => {
      if (todo.id === listId) {
        return {
          ...todo,
          tasks: todo.tasks.map(task =>
            task.id !== taskId ? task : { ...task, done },
          ),
        };
      }

      return todo;
    });

    setTodos(updatedTodo);
  };

  const removeTodoTask = (listId: number, taskId: number) => {
    const updatedTodo = todos.map(todo => {
      if (todo.id === listId) {
        return {
          ...todo,
          tasks: todo.tasks.filter(task => task.id !== taskId),
        };
      }

      return todo;
    });

    setTodos(updatedTodo);
  };

  const options = {
    todos,
    addTodoList,
    removeTodoList,
    addTodoTask,
    updateTodoTaskStatus,
    removeTodoTask,
  };

  return (
    <TodoContext.Provider value={options}>{children}</TodoContext.Provider>
  );
};

export default TodoProvider;
