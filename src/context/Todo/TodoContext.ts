import React from 'react';
import { TodoContextType } from '../../types/todo-context.types';

const contextDefaultValues: TodoContextType = {
  todos: [],
  addTodoList: () => {},
};

const TodoContext = React.createContext<TodoContextType>(contextDefaultValues);

export default TodoContext;
