import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { AppStackParamList } from '../../types/navigator.types';
import {
  CreateListScreen,
  HomeScreen,
  SettingsScreen,
  TodoTasksScreen,
} from '../../screens';

const Stack = createNativeStackNavigator<AppStackParamList>();

const AppNavigator = () => (
  <Stack.Navigator
    initialRouteName="Home"
    screenOptions={{ headerShown: false }}>
    <Stack.Screen name="Home" component={HomeScreen} />
    <Stack.Group
      screenOptions={{ presentation: 'modal', animation: 'slide_from_bottom' }}>
      <Stack.Screen name="CreateList" component={CreateListScreen} />
      <Stack.Screen name="TodoTasks" component={TodoTasksScreen} />
      <Stack.Screen name="Settings" component={SettingsScreen} />
    </Stack.Group>
  </Stack.Navigator>
);

export default AppNavigator;
