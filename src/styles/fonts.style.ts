import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  regular: {
    fontFamily: 'Montserrat-Regular',
  },
  bold: {
    fontFamily: 'Montserrat-Bold',
  },
});
