import COLORS from './colors.style';

export default {
  colors: {
    primary: COLORS.PRIMARY,
    background: COLORS.WHITE,
    text: COLORS.DEFAULT,
  },
};
