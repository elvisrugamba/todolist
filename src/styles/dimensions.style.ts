import { Dimensions } from 'react-native';

export default {
  FONT_SIZE_XS: 12,
  FONT_SIZE_S: 14,
  FONT_SIZE_M: 16,
  FONT_SIZE_L: 20,
  FONT_SIZE_XL: 24,
  SIZE_XS: 12,
  SIZE_S: 14,
  SIZE_M: 16,
  SIZE_L: 20,
  SIZE_XL: 24,
  SCREEN_WIDTH: Dimensions.get('screen').width,
  SCREEN_HEIGHT: Dimensions.get('screen').height,
};
