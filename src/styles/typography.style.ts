import { StyleSheet } from 'react-native';
import Colors from './colors.style';
import Dimensions from './dimensions.style';
import Fonts from './fonts.style';

export default StyleSheet.create({
  headline: {
    ...Fonts.regular,
    color: Colors.DEFAULT,
    fontSize: Dimensions.FONT_SIZE_XL,
  },
  title: {
    ...Fonts.regular,
    color: Colors.DEFAULT,
    fontSize: Dimensions.FONT_SIZE_L,
  },
  body: {
    ...Fonts.regular,
    color: Colors.DEFAULT,
    fontSize: Dimensions.FONT_SIZE_M,
  },
  body_small: {
    ...Fonts.regular,
    color: Colors.DEFAULT,
    fontSize: Dimensions.FONT_SIZE_S,
    lineHeight: 16,
  },
  caption: {
    ...Fonts.regular,
    color: Colors.DEFAULT,
    fontSize: Dimensions.FONT_SIZE_XS,
  },
});
