import React from 'react';
import { View, ViewStyle } from 'react-native';

import Text from '../Text';
import styles from './ListEmpty.styles';

type ListEmptyProps = {
  title: string;
  description: string;
  style?: ViewStyle;
};

const ListEmpty: React.FC<ListEmptyProps> = ({ title, description, style }) => (
  <View style={[styles.container, style]}>
    <Text body bold>
      {title}
    </Text>
    <Text bodySmall>{description}</Text>
  </View>
);

export default ListEmpty;
