import { StyleSheet } from 'react-native';

import { Colors } from '../../styles';

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 30,
    borderBottomWidth: 4,
    borderBottomColor: Colors.DEFAULT,
  },
  left: {
    flex: 1,
    marginRight: 10,
    paddingVertical: 8,
  },
  caption: {
    marginTop: 4,
    marginBottom: 8,
  },
  right: {
    marginRight: 30,
  },
  closeBtn: {
    height: 'auto',
  },
});

export default styles;
