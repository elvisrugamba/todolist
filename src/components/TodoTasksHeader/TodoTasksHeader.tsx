import React from 'react';
import { View, ViewStyle } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';

import Button from '../Button';
import Text from '../Text';
import { Colors } from '../../styles';
import styles from './TodoTasksHeader.styles';

type TodoTasksHeaderProps = {
  title: string;
  completed: number;
  total: number;
  color: string;
  rightHandler: () => void;
  style?: ViewStyle;
};

const TodoTasksHeader: React.FC<TodoTasksHeaderProps> = ({
  title,
  completed,
  total,
  color,
  rightHandler,
  style,
}) => {
  const { t } = useTranslation();

  return (
    <View style={[styles.header, { borderBottomColor: color }, style]}>
      <View style={styles.left}>
        <Text headline bold>
          {title}
        </Text>
        <Text bodySmall bold color={Colors.LIGHT_GRAY} style={styles.caption}>
          {completed} {t('of')} {t('task', { count: total })}
        </Text>
      </View>
      <View style={styles.right}>
        <Button transparent onPress={rightHandler} style={styles.closeBtn}>
          <Icon name="close" color={Colors.DEFAULT} size={28} />
        </Button>
      </View>
    </View>
  );
};

export default TodoTasksHeader;
