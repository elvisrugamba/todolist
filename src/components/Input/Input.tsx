import React from 'react';
import {
  View,
  TextInput,
  TextStyle,
  ViewStyle,
  TextInputProps,
  ViewProps,
} from 'react-native';

import Text from '../Text';
import { Colors } from '../../styles';
import styles from './Input.styles';

export type InputProps = TextInputProps &
  ViewProps & {
    label?: string;
    placeholder?: string;
    placeholderTextColor?: string;
    error?: string;
    width?: number | string;
    height?: number | string;
    textStyles?: TextStyle;
    labelStyles?: TextStyle;
    style?: ViewStyle;
  };

const Input: React.FC<InputProps> = ({
  label,
  placeholder,
  placeholderTextColor,
  error,
  width,
  height,
  textStyles,
  labelStyles,
  style,
  ...rest
}) => {
  const inputViewStyles: ViewStyle[] | any = [
    styles.inputStyle,
    styles.inputContainer,
    width && { width },
    height && { height },
    error && { borderColor: Colors.ERROR },
  ];

  const inputStyles: ViewStyle[] | any = [
    styles.inputView,
    styles.inputText,
    textStyles,
  ];

  const lebelContent = label && (
    <Text body bold style={[styles.label, labelStyles]}>
      {label}
    </Text>
  );

  const errorContent = error && <Text style={styles.errorText}>{error}</Text>;

  return (
    <View style={[styles.item, style]}>
      {lebelContent}
      <View style={inputViewStyles}>
        <TextInput
          style={inputStyles}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor || Colors.LIGHT_GRAY}
          selectionColor={Colors.PRIMARY}
          underlineColorAndroid="transparent"
          {...rest}
        />
      </View>
      {error && errorContent}
    </View>
  );
};

export default Input;
