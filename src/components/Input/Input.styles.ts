import { StyleSheet } from 'react-native';

import { Colors, Typography } from '../../styles';

const styles = StyleSheet.create({
  item: {
    marginVertical: 10,
    alignContent: 'center',
  },
  inputStyle: {
    height: 48,
    width: '100%',
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 8,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: Colors.LIGHT_GRAY,
  },
  inputText: {
    ...Typography.body,
    textDecorationColor: 'transparent',
    textShadowColor: 'transparent',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  inputView: {
    flex: 1,
  },
  label: {
    ...Typography.body,
    marginBottom: 8,
  },
  errorText: {
    ...Typography.caption,
    color: Colors.ERROR,
  },
});

export default styles;
