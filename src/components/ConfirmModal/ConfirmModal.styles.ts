import { StyleSheet } from 'react-native';

import { Colors, Fonts } from '../../styles';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backDrop: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
  modalView: {
    backgroundColor: Colors.WHITE,
    margin: 16,
    paddingHorizontal: 8,
    paddingTop: 8,
    borderRadius: 8,
  },
  cardItem: {
    padding: 8,
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
  },
  button: {
    height: 38,
    marginHorizontal: 4,
  },
  buttonText: {
    ...Fonts.bold,
    textTransform: 'uppercase',
  },
  marginTop: {
    marginTop: 8,
  },
  marginBottom: {
    marginBottom: 8,
  },
});

export default styles;
