import React from 'react';
import { View, Modal, Pressable } from 'react-native';
import { useTranslation } from 'react-i18next';

import Text from '../Text';
import Button from '../Button';
import { Colors } from '../../styles';
import styles from './ConfirmModal.styles';

type ConfirmModalProps = {
  visible: boolean;
  title: string;
  description: string;
  close: () => void;
  action: () => void;
};

const ConfirmModal: React.FC<ConfirmModalProps> = ({
  visible,
  title,
  description,
  close,
  action,
}) => {
  const { t } = useTranslation();
  const onAction = () => {
    action();
    close();
  };

  return (
    <Modal
      visible={visible}
      animationType="fade"
      transparent={true}
      statusBarTranslucent
      onRequestClose={() => close()}>
      <Pressable onPress={close} style={styles.backDrop}>
        <View style={styles.modalView}>
          <View style={styles.cardItem}>
            <Text body bold style={styles.marginBottom}>
              {title}
            </Text>
            <Text bodySmall>{description}</Text>
          </View>
          <View style={styles.buttons}>
            <View style={styles.flex}>
              <Button
                transparent
                textStyle={styles.buttonText}
                style={styles.button}
                onPress={close}>
                {t('cancel')}
              </Button>
            </View>
            <View style={styles.flex}>
              <Button
                transparent
                color={Colors.ERROR}
                textStyle={styles.buttonText}
                style={styles.button}
                onPress={onAction}>
                {t('remove')}
              </Button>
            </View>
          </View>
        </View>
      </Pressable>
    </Modal>
  );
};

export default ConfirmModal;
