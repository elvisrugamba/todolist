import React, { useState } from 'react';
import { TouchableOpacity, ViewStyle } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Swipeable, {
  SwipeableProps,
} from 'react-native-gesture-handler/Swipeable';
import { useTranslation } from 'react-i18next';

import Text from '../Text';
import { Colors, Fonts } from '../../styles';
import styles from './TaskItem.styles';
import Button from '../Button';

type TaskItemProps = {
  description: string;
  done: boolean;
  onToggle: (done: boolean) => void;
  onLongPress?: () => void;
  style?: ViewStyle;
};

const TaskItem: React.FC<TaskItemProps> = ({
  description,
  done,
  onToggle,
  onLongPress,
  style,
}) => {
  const [value, setValue] = useState(done);
  const { t } = useTranslation();

  const toggle = () => {
    setValue(!value);
    onToggle(!value);
  };

  // Use swipeable, interpolate opacity of delete button(right action)

  const renderRightActions: SwipeableProps['renderRightActions'] = () => {
    // const opacity = dragX.interpolate({
    //   inputRange: [0, 100],
    //   outputRange: [0, 1],
    // });

    return (
      <Button
        color={Colors.ERROR}
        textStyle={Fonts.bold}
        style={styles.deleteBtn}>
        {t('delete')}
      </Button>
    );
  };

  const onSwipeableRightOpen = (direction: string) => {
    console.log('Swipe from right', direction);
  };

  return (
    <Swipeable
      renderRightActions={renderRightActions}
      onSwipeableOpen={onSwipeableRightOpen}>
      <TouchableOpacity
        activeOpacity={0.8}
        onLongPress={onLongPress}
        style={[styles.item, style]}>
        <Button transparent style={styles.toggleBtn} onPress={toggle}>
          <Icon
            name={value ? 'square' : 'square-outline'}
            color={value ? Colors.LIGHT_GRAY : Colors.DEFAULT}
            size={16}
          />
        </Button>
        <Text body bold color={value ? Colors.LIGHT_GRAY : Colors.DEFAULT}>
          {description}
        </Text>
      </TouchableOpacity>
    </Swipeable>
  );
};

export default TaskItem;
