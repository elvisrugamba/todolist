import { StyleSheet } from 'react-native';

import { Fonts } from '../../styles';

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    paddingVertical: 16,
  },
  toggleBtn: {
    height: 'auto',
    marginRight: 10,
  },
  deleteBtn: {
    height: 'auto',
    width: 80,
    paddingVertical: 16,
  },
});

export default styles;
