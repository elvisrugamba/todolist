import { StyleSheet } from 'react-native';

import { Colors, Dimensions } from '../../styles';

const styles = StyleSheet.create({
  listItem: {
    alignItems: 'center',
    justifyContent: 'space-between',
    width: Dimensions.SCREEN_WIDTH * 0.5,
    height: Dimensions.SCREEN_HEIGHT * 0.3,
    backgroundColor: Colors.PRIMARY,
    borderRadius: 4,
    paddingHorizontal: 16,
    paddingVertical: 30,
    marginHorizontal: 10,
  },
});

export default styles;
