import React from 'react';
import { TouchableOpacity, ViewStyle } from 'react-native';
import { useTranslation } from 'react-i18next';

import Text from '../Text';
import { Colors } from '../../styles';
import styles from './ListItem.styles';
import { Status } from './components';

type ListItemProps = {
  name: string;
  remaining: number;
  completed: number;
  color: string;
  onPress?: () => void;
  onLongPress?: () => void;
  style?: ViewStyle;
};

const ListItem: React.FC<ListItemProps> = ({
  name,
  remaining,
  completed,
  color,
  onPress,
  onLongPress,
  style,
}) => {
  const { t } = useTranslation();

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      onLongPress={onLongPress}
      style={[styles.listItem, { backgroundColor: color }, style]}>
      <Text title bold color={Colors.WHITE} numberOfLines={1}>
        {name}
      </Text>
      <Status label={t('remaining')} value={remaining} />
      <Status label={t('completed')} value={completed} />
    </TouchableOpacity>
  );
};

export default ListItem;
