import React from 'react';
import { View } from 'react-native';

import Text from '../../../Text';
import { Colors } from '../../../../styles';
import styles from './Status.styles';

type StatusProps = {
  label: string;
  value: number;
};

const Status: React.FC<StatusProps> = ({ label, value }) => (
  <View style={styles.status}>
    <Text size={32} color={Colors.WHITE}>
      {value}
    </Text>
    <Text caption bold color={Colors.WHITE}>
      {label}
    </Text>
  </View>
);

export default Status;
