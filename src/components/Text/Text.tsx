import React from 'react';
import { Text as RNText, TextProps, TextStyle } from 'react-native';

import { Fonts, Typography } from '../../styles';

import styles from './Text.styles';

export type TypographyProps = TextProps & {
  headline?: boolean;
  title?: boolean;
  body?: boolean;
  bodySmall?: boolean;
  caption?: boolean;
  size?: number;
  color?: string;
  bold?: boolean;
  center?: boolean;
  style?: TextStyle;
};

const Text: React.FC<TypographyProps> = ({
  headline,
  title,
  body,
  bodySmall,
  caption,
  size,
  color,
  bold,
  center,
  style,
  children,
  ...rest
}) => {
  const textStyle: TextStyle | any = [
    styles.default,
    headline && Typography.headline,
    title && Typography.title,
    body && Typography.body,
    bodySmall && Typography.body_small,
    caption && Typography.caption,
    size && { fontSize: size },
    color && { color },
    bold && Fonts.bold,
    center && styles.center,
    style && style,
  ];

  return (
    <RNText style={textStyle} {...rest}>
      {children}
    </RNText>
  );
};

export default Text;
