import { StyleSheet } from 'react-native';
import { Colors, Dimensions, Fonts } from '../../styles';

const styles = StyleSheet.create({
  default: {
    fontSize: Dimensions.FONT_SIZE_M,
    color: Colors.DEFAULT,
    ...Fonts.regular,
  },
  center: { textAlign: 'center' },
});

export default styles;
