import { StyleSheet } from 'react-native';

import { Colors, Typography } from '../../styles';

const styles = StyleSheet.create({
  default: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    backgroundColor: Colors.PRIMARY,
    borderRadius: 4,
  },
  defaultText: {
    ...Typography.body_small,
    color: Colors.WHITE,
    lineHeight: 16,
  },
  primary: {
    backgroundColor: Colors.PRIMARY,
  },
  transparent: {
    backgroundColor: Colors.TRANSPARENT,
  },
  outline: {
    backgroundColor: Colors.WHITE,
    borderWidth: 2,
    borderColor: Colors.LIGHT_GRAY,
  },
});

export default styles;
