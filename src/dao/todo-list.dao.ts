import { Connection, Repository } from 'typeorm/browser';

import { TodoList } from '../models/todo-list.model';

export class TodoListDao {
  private repository: Repository<TodoList>;

  constructor(connection: Connection) {
    this.repository = connection.getRepository(TodoList);
  }

  async getTodoLists(): Promise<TodoList[]> {
    const lists = await this.repository.find();

    return lists;
  }
}
