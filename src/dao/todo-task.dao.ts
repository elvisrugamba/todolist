import { Connection, Repository } from 'typeorm/browser';

import { TodoTask } from '../models/todo-task.model';

export class TodoTaskDao {
  private repository: Repository<TodoTask>;

  constructor(connection: Connection) {
    this.repository = connection.getRepository(TodoTask);
  }

  async getTodoTasks(listId: number): Promise<TodoTask[]> {
    console.log(listId);

    const tasks = await this.repository.find({ where: { list: listId } });

    return tasks;
  }
}
