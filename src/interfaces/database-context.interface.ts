import { TodoListDao } from '../dao/todo-list.dao';
import { TodoTaskDao } from '../dao/todo-task.dao';

export interface DatabaseContextData {
  todoListDao: TodoListDao;
  todoTaskDao: TodoTaskDao;
}
