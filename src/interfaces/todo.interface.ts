export interface Task {
  id: number;
  description: string;
  done: boolean;
  createdDate: Date;
}

export interface List {
  id: number;
  name: string;
  tasks: Task[];
  color: string;
  createdDate: Date;
}
