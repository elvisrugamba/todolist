export interface ListFormValues {
  name: string;
  color: string;
}

export interface TaskFormValues {
  description: string;
}
